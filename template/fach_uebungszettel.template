%!TEX TS-program = xelatex

% in case of the ERROR: 'unexpected "\\" expecting letter' make sure to escape all
% <DOLLARSIGN> in the document (even here in the comments) that are not part of the pandoc
% template syntax with another <DOLLARSIGN> (so <DOLLARSIGN> -> <DOLLARSIGN><DOLLARSIGN>)

% prevent overfull lines
\setlength{\emergencystretch}{3em}
% prevent ERROR: '! Undefined control sequence. l.42 \tightlist'
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

% If any other error of the following type appear:
% '! LaTeX Error: Environment XXX undefined.'
% Try to search and copy the missing definition from here:
% https://github.com/jgm/pandoc-templates/blob/master/default.latex

% creates header from an external latex file
% (BEWARE: path must be relative to the location of the makefile that invokes pandoc)
\input{template/header}

% make markdown tables possible
\usepackage{longtable,booktabs}

\newcommand{\passthrough}[1]{#1}

% make section numbering removable (removed by default)
$if(numbersections)$
\setcounter{secnumdepth}{$if(secnumdepth)$$secnumdepth$$else$5$endif$}
$else$
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
$endif$

% better graphics handling
$if(graphics)$
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
$endif$

% enable header imports
$for(header-includes)$
$header-includes$
$endfor$

\newcommand{\dozent}{$lecturer$}				% <-- name of the lecturer
\newcommand{\tutor}{$tutor$}					% <-- name of the tutor
\newcommand{\tutoriumNo}{$tutorialnum$}			% <-- tutorial number (look up in KVV)
\newcommand{\ubungNo}{$exercisenum$}			% <-- number of the exercise
\newcommand{\veranstaltung}{$coursename$}		% <-- name of the course
\newcommand{\semester}{$semyear$}				% <-- e.g. SoSe 17, WiSe 17/18
\newcommand{\studenten}{$author$}				% <-- name(s) of the author(s)

\begin{document}

% creates title page from an external latex file
% (BEWARE: path must be relative to the location of the makefile that invokes pandoc)
\input{template/titlepage}

% This is where pandoc puts its rendering of your text (in the place of
% the variable `body`)
$body$

\end{document}
