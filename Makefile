# Convert all .md-suffixed files in the working directory
SOURCE_DOCS = $(wildcard *.md)

# name of the PDF-conversion engine (must be installed!)
TEXENGINE = xelatex

# the pandoc template-file to use
TEMPLATE = template/fach_uebungszettel.template

# if the paper should be wider, put the following as an option after the pandoc call:
LANDSCAPEMODE = -V geometry:"paperwidth=15in, paperheight=210mm, margin=2cm"

EXPORTED_DOCS = $(SOURCE_DOCS:.md=.pdf)

%.pdf: %.md
	pandoc -s $< --pdf-engine=$(TEXENGINE) --listings --template=$(TEMPLATE) -o $@

# without xelatex and the custom template the following would suffice:
#	pandoc $(PFLAGS) $@ $<

.PRECIOUS: $(SOURCE_DOCS)

.PHONY: all clean

all : $(EXPORTED_DOCS)

clean:
	-rm -f $(EXPORTED_DOCS)
