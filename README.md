# FU Pandoc Template

This is a Pandoc template made from the latex template of the FU Latex-Crashkurs.

To create the PDF-files from **all** markdown files in the working directory (even the `README.md` but that should not be present in a real working directoy anyways), just use `make`.

## Requirements

* make
* pandoc
* texlive
* texlive-xetex
* texlive-lang-german
* texlive-latex-extra
* .. maybe some other latex packages that i may have forgotten
